﻿//https://www.youtube.com/watch?v=JhaQq7tbbKU
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorConsola.Services
{
    public static class CSVHelper
    {
        /// <summary>
        /// Lee un archivo CSV y regresa una lista de objetos.
        /// </summary>
        /// <typeparam name="T">Tipo de objeto</typeparam>
        /// <param name="archivo">Nombre del Archivo</param>
        /// <returns>Lista de objetos</returns>
        public static List<T> ObtenerDatos<T>(string archivo) where T : class
        {
            try
            {
                List<T> datos = new List<T>();
                using (var reader = new StreamReader(archivo))
                {
                    using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {
                        csv.Read();
                        csv.ReadHeader();
                        while (csv.Read())
                        {
                            datos.Add(csv.GetRecord<T>());
                        }
                    }
                }
                return datos;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
            
        }
        /// <summary>
        /// Crea un archivo CSV en base a una lista de objetos dada
        /// </summary>
        /// <typeparam name="T">Tipo de objeto</typeparam>
        /// <param name="archivo">Ruta del archivo a crear</param>
        /// <param name="datos">Datos con los cuales crear el CSV</param>
        /// <returns>Conformiación del archivo creado</returns>
        public static bool CrearCSV<T>(string archivo, List<T> datos) where T : class
        {
            try
            {
                using (var write = new StreamWriter("B" + archivo))
                {
                    using (var csv = new CsvWriter(write, CultureInfo.InvariantCulture))
                    {
                        csv.WriteRecords(datos);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
