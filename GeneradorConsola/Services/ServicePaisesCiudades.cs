﻿//https://www.universal-tutorial.com/rest-apis/free-rest-api-for-country-state-city
using CsvHelper.Configuration.Attributes;
using GeneradorConsola.Entidades;
using GeneradorConsola.Modelos;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorConsola.Services
{
    public static class ServicePaisesCiudades
    {
        static string ObtenerToken()
        {
            var client = new RestClient("https://www.universal-tutorial.com/api/getaccesstoken");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            request.AddHeader("api-token", "e4UXSoZLRTpQcrmR8VNp1yKaQ_bx6KCaYMioOtZ1X0D0lpbEgk0jW5sGSrRFwV4plW8");
            request.AddHeader("user-email", "arturo.espinoza.dti@imfe.mx");
            var response = client.Get(request);
            return JsonConvert.DeserializeObject<ModelToken>(response.Content).auth_token;
        }

        public static List<Pais> ObtenerPaises()
        {
            string token = ObtenerToken();
            var client = new RestClient("https://www.universal-tutorial.com/api/countries");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", "Bearer " + token);
            var response = client.Get(request);
            var datos = JsonConvert.DeserializeObject<List<ModelPais>>(response.Content);
            List<Pais> paises = new List<Pais>();
            int i = 0;
            foreach (var item in datos)
            {
                paises.Add(new Pais()
                {
                    IdPais = Guid.NewGuid().ToString(),
                    Nombre = item.country_name,
                    NombreCorto = item.country_short_name
                });
            }
            return paises;
        }

        public static List<Estadio> GenerarEstadios(int numero, string pais, string idMundial)
        {
            Random r = new Random(DateTime.Now.Millisecond);
            string token = ObtenerToken();
            List<Estadio> estadios = new List<Estadio>();
            var client = new RestClient($"https://www.universal-tutorial.com/api/states/{pais}");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", "Bearer " + token);
            var response = client.Get(request);
            var datos = JsonConvert.DeserializeObject<List<ModelCiudad>>(response.Content);
            string ciudad = "";
            if (numero >= datos.Count)
            {
                foreach (var item in datos)
                {
                    estadios.Add(new Estadio()
                    {
                        Capacidad = r.Next(40, 80) * 1000,
                        Ciudad = item.state_name,
                        IdEstadio = Guid.NewGuid().ToString(),
                        IdMundial = idMundial
                    });
                }
            }
            else
            {
                for (int i = 0; i < numero; i++)
                {
                    do
                    {
                        ciudad = datos[r.Next(datos.Count - 1)].state_name;
                    } while (estadios.Where(e => e.Ciudad == ciudad).Count() > 0);
                    estadios.Add(new Estadio()
                    {
                        Capacidad = r.Next(40, 80) * 1000,
                        Ciudad = ciudad,
                        IdEstadio = Guid.NewGuid().ToString(),
                        IdMundial = idMundial
                    });
                }
            }
            return estadios;
        }
    }
}
