﻿using GeneradorConsola.Entidades;
using GeneradorConsola.Services;
using System.IO.Pipes;
using System.Threading.Tasks;

namespace GeneradorConsola
{
    internal class Program
    {
        
        static Random r = new Random(DateTime.Now.Millisecond);
        static List<Pais> paises = new List<Pais>();
        static List<Mundial> mundiales = new List<Mundial>();
        static List<Grupo> grupos = new List<Grupo>();
        static List<Asistente> asistentes = new List<Asistente>();
        static List<Estadio> estadios = new List<Estadio>();
        static List<Incidente> incidentes = new List<Incidente>();
        static List<Partido> partidos = new List<Partido>();
        static List<string> letras = new List<string>() { "A", "B", "C", "D", "E", "F", "G", "H" };
        static Dictionary<int, string> tipoIncidente = new Dictionary<int, string>()
        {
            {1,"Pelea" },
            {2,"Accidente de trafico" },
            {3,"Incendio" },
            {4,"Inundación" },
            {5,"Accidente contrucción" }
        };
        static Mundial mundialActual;
        static void Main(string[] args)
        {
            DateTime tiempoInicio = DateTime.Now;
            Console.WriteLine("Generador de datos de mundiales");
            Console.WriteLine("Generando lista de paises...");
            paises = ServicePaisesCiudades.ObtenerPaises();
            Console.WriteLine("Guardando en CSV");
            CSVHelper.CrearCSV("Paises.csv", paises);

            Console.WriteLine("Generando datos de mundiales...");
            mundiales = new List<Mundial>();
            List<int> anios = new List<int>();
            for (int i = 1930; i <= 2022; i += 4)
            {
                int anio = i;
                DateTime inicio = new DateTime(anio, 6, r.Next(1, 30));
                mundialActual = new Mundial()
                {
                    Anio = anio,
                    IdMundial = Guid.NewGuid().ToString(),
                    Pais = ObjetoPaisAleatorio().Nombre,
                    FechaInicio = inicio,
                    FechaFin = inicio.AddDays(30)
                };
                mundiales.Add(mundialActual);
            }


            Console.WriteLine("Generando Grupos por mundial...");
            foreach (var mundial in mundiales)
            {
                grupos = new List<Grupo>();
                partidos = new List<Partido>();
                Console.WriteLine($"Generando datos del mundial de {mundial.Pais} del {mundial.Anio}...");
                //estadios
                estadios = ServicePaisesCiudades.GenerarEstadios(r.Next(5, 20), mundial.Pais, mundial.IdMundial);
                CSVHelper.CrearCSV($"EstadiosMundial{mundial.Anio}.csv", estadios);
                //fase de grupos
                List<Grupo> gruposDeMundial = GenerarGruposEnPorMundial(mundial, estadios);
                //8vos de final
                Partido p1 = GeneraPartido(gruposDeMundial[0].IdPaisPrimero, gruposDeMundial[0].Primero, gruposDeMundial[1].IdPaisSegundo, gruposDeMundial[1].Segundo, "8vos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(12));
                Partido p2 = GeneraPartido(gruposDeMundial[1].IdPaisPrimero, gruposDeMundial[1].Primero, gruposDeMundial[0].IdPaisSegundo, gruposDeMundial[0].Segundo, "8vos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(13));
                Partido p3 = GeneraPartido(gruposDeMundial[2].IdPaisPrimero, gruposDeMundial[2].Primero, gruposDeMundial[3].IdPaisSegundo, gruposDeMundial[3].Segundo, "8vos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(14));
                Partido p4 = GeneraPartido(gruposDeMundial[3].IdPaisPrimero, gruposDeMundial[3].Primero, gruposDeMundial[2].IdPaisSegundo, gruposDeMundial[2].Segundo, "8vos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(15));

                Partido p5 = GeneraPartido(gruposDeMundial[4].IdPaisPrimero, gruposDeMundial[4].Primero, gruposDeMundial[5].IdPaisSegundo, gruposDeMundial[5].Segundo, "8vos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(16));
                Partido p6 = GeneraPartido(gruposDeMundial[5].IdPaisPrimero, gruposDeMundial[5].Primero, gruposDeMundial[4].IdPaisSegundo, gruposDeMundial[4].Segundo, "8vos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(17));
                Partido p7 = GeneraPartido(gruposDeMundial[6].IdPaisPrimero, gruposDeMundial[6].Primero, gruposDeMundial[7].IdPaisSegundo, gruposDeMundial[7].Segundo, "8vos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(18));
                Partido p8 = GeneraPartido(gruposDeMundial[7].IdPaisPrimero, gruposDeMundial[7].Primero, gruposDeMundial[6].IdPaisSegundo, gruposDeMundial[6].Segundo, "8vos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(19));
                partidos.Add(p1);
                partidos.Add(p2);
                partidos.Add(p3);
                partidos.Add(p4);
                partidos.Add(p5);
                partidos.Add(p6);
                partidos.Add(p7);
                partidos.Add(p8);
                //4tos de final
                Partido q1 = GeneraPartido(p1.IdPaisGanador, p1.PaisGanador, p2.IdPaisGanador, p2.PaisGanador, "4tos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(20));
                Partido q2 = GeneraPartido(p3.IdPaisGanador, p3.PaisGanador, p4.IdPaisGanador, p4.PaisGanador, "4tos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(21));
                Partido q3 = GeneraPartido(p5.IdPaisGanador, p5.PaisGanador, p6.IdPaisGanador, p6.PaisGanador, "4tos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(22));
                Partido q4 = GeneraPartido(p7.IdPaisGanador, p7.PaisGanador, p8.IdPaisGanador, p8.PaisGanador, "4tos", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(23));
                partidos.Add(q1);
                partidos.Add(q2);
                partidos.Add(q3);
                partidos.Add(q4);
                //semifinales
                Partido s1 = GeneraPartido(q1.IdPaisGanador, q1.PaisGanador, q2.IdPaisGanador, q2.PaisGanador, "Semifinales", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(24));
                Partido s2 = GeneraPartido(q3.IdPaisGanador, q3.PaisGanador, q4.IdPaisGanador, q4.PaisGanador, "Semifinales", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(25));
                partidos.Add(s1);
                partidos.Add(s2);
                //Final
                Partido f1 = GeneraPartido(s1.IdPaisGanador, s1.PaisGanador, s2.IdPaisGanador, s2.PaisGanador, "Final", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(30));
                Partido f2 = GeneraPartido(s1.IdPaisPerdedor, s1.PaisPerdedor, s2.IdPaisPerdedor, s2.PaisPerdedor, "Final", false, mundial.IdMundial, estadios[r.Next(estadios.Count - 1)], mundial.FechaInicio.AddDays(29));
                partidos.Add(f1);
                partidos.Add(f2);
                mundial.IdGanador = f1.IdPaisPerdedor;
                mundial.Ganador = f1.PaisGanador;
                mundial.IdSegundo = f1.IdPaisPerdedor;
                mundial.Segundo = f1.PaisPerdedor;
                mundial.IdTercero = f2.IdPaisGanador;
                mundial.Tercero = f2.PaisGanador;
                CSVHelper.CrearCSV($"GruposMundial{mundial.Anio}.csv", gruposDeMundial);
                CSVHelper.CrearCSV($"PartidosMundual{mundial.Anio}.csv", partidos);
                asistentes = new List<Asistente>();
                incidentes = new List<Incidente>();
                foreach (var p in partidos)
                {
                    Estadio estadio = estadios.Find(e => e.IdEstadio == p.IdEstadio);
                    for (int i = 0; i < p.Asistentes; i++)
                    {
                        asistentes.Add(new Asistente()
                        {
                            Edad = r.Next(4, 70),
                            Genero = r.NextDouble() > .5 ? 'M' : 'F',
                            IdAsistente = Guid.NewGuid().ToString(),
                            IdPartido = p.IdPartido,
                            FechaLlegada = p.FechaHora.AddDays(-r.Next(30)),
                            FechaPartida = p.FechaHora.AddDays(r.Next(30))
                        });
                    }
                }
                int porcentaIncidentes = r.Next(100);
                int numPartidosConIncidentes = porcentaIncidentes * partidos.Count / 100;
                mundial.NumIncidentes = numPartidosConIncidentes;
                List<Partido> partidosConIncidentes = new List<Partido>();
                for (int i = 0; i < numPartidosConIncidentes; i++)
                {
                    Partido pIncidente;
                    do
                    {
                        pIncidente = partidos[r.Next(partidos.Count - 1)];
                    } while (partidosConIncidentes.Contains(pIncidente));
                    partidosConIncidentes.Add(pIncidente);
                    incidentes.Add(new Incidente()
                    {
                        Detenidos = r.Next(pIncidente.Asistentes / 20),
                        Hospitalizados = r.Next(pIncidente.Asistentes / 40),
                        Fecha = pIncidente.FechaHora,
                        IdIncidente = Guid.NewGuid().ToString(),
                        IdMundial = mundial.IdMundial,
                        IdPartido = pIncidente.IdPartido,
                        Nivel = tipoIncidente[r.Next(1, 5)].ToString()
                    });
                }
                CSVHelper.CrearCSV($"AsistentesMundial{mundial.Anio}.csv", asistentes);
                CSVHelper.CrearCSV($"IncidentesMundial{mundial.Anio}.csv", incidentes);
            }

            CSVHelper.CrearCSV("Mundiales.csv", mundiales);

            TimeSpan totalTiempo = DateTime.Now - tiempoInicio;
            Console.Write($"Proceso Terminado con un tiempo total de: {totalTiempo.ToString()}");
        }

        private static List<Grupo> GenerarGruposEnPorMundial(Mundial mundial, List<Estadio> estadios)
        {
            //Generar lista de paises
            List<Pais> paisesClasificados = new List<Pais>();
            List<Grupo> grupos = new List<Grupo>();
            for (int i = 0; i < 32; i++)
            {
                Pais p;
                do
                {
                    p = ObjetoPaisAleatorio();
                } while (paisesClasificados.Contains(p));
                paisesClasificados.Add(p);
            }
            int index = 0;
            int grupoIndex = 0;
            for (int i = 0; i < 8; i++)
            {
                Grupo g = new Grupo();

                g.IdMundial = mundial.IdMundial;
                g.IdGrupo = Guid.NewGuid().ToString();
                g.NombreGrupo = letras[grupoIndex];
                g.IdPais1 = paisesClasificados[index].IdPais;
                g.Pais1 = paisesClasificados[index].Nombre;
                index++;
                g.IdPais2 = paisesClasificados[index].IdPais;
                g.Pais2 = paisesClasificados[index].Nombre;
                index++;
                g.IdPais3 = paisesClasificados[index].IdPais;
                g.Pais3 = paisesClasificados[index].Nombre;
                index++;
                g.IdPais4 = paisesClasificados[index].IdPais;
                g.Pais4 = paisesClasificados[index].Nombre;
                index++;
                grupoIndex++;
                GeneraPartidosPorGrupo(g, estadios);
                grupos.Add(g);
            }
            return grupos;
        }

        private static void GeneraPartidosPorGrupo(Grupo g, List<Estadio> estadios)
        {
            int pp1 = 0, pp2 = 0, pp3 = 0, pp4 = 0;
            DateTime fecha = new DateTime(mundialActual.FechaInicio.Year, mundialActual.FechaInicio.Month, mundialActual.FechaInicio.Day, r.Next(8, 20), 0, 0);
            Partido p1 = GeneraPartido(g.IdPais1, g.Pais1, g.IdPais2, g.Pais2, "Grupos", true, g.IdMundial, estadios[r.Next(estadios.Count - 1)], fecha);
            if (p1.FueEmpate)
            {
                pp1 += 1;
                pp2 += 1;
            }
            else
            {
                if (g.IdPais1 == p1.IdPaisGanador)
                {
                    pp1 += 3;
                }
                else
                {
                    pp2 += 3;
                }
            }
            Partido p2 = GeneraPartido(g.IdPais1, g.Pais1, g.IdPais3, g.Pais3, "Grupos", true, g.IdMundial, estadios[r.Next(estadios.Count - 1)], fecha.AddDays(2));
            if (p2.FueEmpate)
            {
                pp1 += 1;
                pp3 += 1;
            }
            else
            {
                if (g.IdPais1 == p1.IdPaisGanador)
                {
                    pp1 += 3;
                }
                else
                {
                    pp3 += 3;
                }
            }
            Partido p3 = GeneraPartido(g.IdPais1, g.Pais1, g.IdPais4, g.Pais4, "Grupos", true, g.IdMundial, estadios[r.Next(estadios.Count - 1)], fecha.AddDays(4));
            if (p3.FueEmpate)
            {
                pp1 += 1;
                pp4 += 1;
            }
            else
            {
                if (g.IdPais1 == p1.IdPaisGanador)
                {
                    pp1 += 3;
                }
                else
                {
                    pp4 += 3;
                }
            }
            Partido p4 = GeneraPartido(g.IdPais2, g.Pais2, g.IdPais3, g.Pais3, "Grupos", true, g.IdMundial, estadios[r.Next(estadios.Count - 1)], fecha.AddDays(6));
            if (p4.FueEmpate)
            {
                pp2 += 1;
                pp3 += 1;
            }
            else
            {
                if (g.IdPais2 == p1.IdPaisGanador)
                {
                    pp2 += 3;
                }
                else
                {
                    pp3 += 3;
                }
            }
            Partido p5 = GeneraPartido(g.IdPais2, g.Pais2, g.IdPais4, g.Pais4, "Grupos", true, g.IdMundial, estadios[r.Next(estadios.Count - 1)], fecha.AddDays(8));
            if (p1.FueEmpate)
            {
                pp2 += 1;
                pp4 += 1;
            }
            else
            {
                if (g.IdPais2 == p1.IdPaisGanador)
                {
                    pp2 += 3;
                }
                else
                {
                    pp4 += 3;
                }
            }
            Partido p6 = GeneraPartido(g.IdPais3, g.Pais3, g.IdPais4, g.Pais4, "Grupos", true, g.IdMundial, estadios[r.Next(estadios.Count - 1)], fecha.AddDays(10));
            if (p6.FueEmpate)
            {
                pp3 += 1;
                pp4 += 1;
            }
            else
            {
                if (g.IdPais3 == p1.IdPaisGanador)
                {
                    pp3 += 3;
                }
                else
                {
                    pp4 += 3;
                }
            }
            partidos.Add(p1);
            partidos.Add(p2);
            partidos.Add(p3);
            partidos.Add(p4);
            partidos.Add(p5);
            partidos.Add(p6);
            g.PuntosPais1 = pp1;
            g.PuntosPais2 = pp2;
            g.PuntosPais3 = pp3;
            g.PuntosPais4 = pp4;
            Dictionary<Pais, int> datos = new Dictionary<Pais, int>();
            datos.Add(paises.Where(x => x.IdPais == g.IdPais1).Single(), pp1);
            datos.Add(paises.Where(x => x.IdPais == g.IdPais2).Single(), pp2);
            datos.Add(paises.Where(x => x.IdPais == g.IdPais3).Single(), pp3);
            datos.Add(paises.Where(x => x.IdPais == g.IdPais4).Single(), pp4);
            var datosordenados = datos.OrderByDescending(d => d.Value).ToList();
            g.IdPaisPrimero = datosordenados[0].Key.IdPais;
            g.IdPaisSegundo = datosordenados[1].Key.IdPais;
            g.Primero = datosordenados[0].Key.Nombre;
            g.Segundo = datosordenados[1].Key.Nombre;
        }

        private static Partido GeneraPartido(string idPais1, string pais1, string idPais2, string pais2, string ronda, bool puedeEmpatar, string idMundial, Estadio estadio, DateTime fecha)
        {
            Partido p = new Partido();
            p.IdMundial = idMundial;
            p.IdPartido = Guid.NewGuid().ToString();
            p.CiudadEstadio = estadio.Ciudad;
            p.IdEstadio = estadio.IdEstadio;
            p.FechaHora = fecha;
            p.Asistentes = r.Next(50, 100) * estadio.Capacidad / 100;
            p.Ronda = ronda;
            p.IdPais1 = idPais1;
            p.Pais1 = pais1;
            p.IdPais2 = idPais2;
            p.Pais2 = pais2;
            p.GolesPais1 = r.Next(0, 10);
            if (puedeEmpatar)
            {
                p.GolesPais2 = r.Next(0, 10);
            }
            else
            {
                do
                {
                    p.GolesPais2 += r.Next(0, 10);
                } while (p.GolesPais1 == p.GolesPais2);
            }
            p.FueEmpate = p.GolesPais1 == p.GolesPais2;
            if (!p.FueEmpate)
            {
                if (p.GolesPais1 > p.GolesPais2)
                {
                    p.PaisGanador = p.Pais1;
                    p.IdPaisGanador = p.IdPais1;
                    p.PaisPerdedor = p.Pais2;
                    p.IdPaisPerdedor = p.IdPais2;
                }
                else
                {
                    p.PaisGanador = p.Pais2;
                    p.IdPaisGanador = p.IdPais2;
                    p.PaisPerdedor = p.Pais1;
                    p.IdPaisPerdedor = p.IdPais1;
                }
            }
            else
            {
                p.IdPaisGanador = "";
                p.PaisGanador = "";
                p.PaisPerdedor = "";
                p.IdPaisPerdedor = "";
            }
            return p;
        }

        private static Pais ObjetoPaisAleatorio()
        {
            return paises[r.Next(paises.Count)];
        }
    }
}