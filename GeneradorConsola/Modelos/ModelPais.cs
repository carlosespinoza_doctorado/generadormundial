﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorConsola.Modelos
{
    public class ModelPais
    {
        public string country_name { get; set; }
        public string country_short_name { get; set; }
        public int country_phone_code { get; set; }
    }
}
