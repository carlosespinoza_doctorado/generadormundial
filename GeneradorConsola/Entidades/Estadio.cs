﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorConsola.Entidades
{
    public class Estadio
    {
        public string IdEstadio { get; set; }
        public string IdMundial { get; set; }
        public int Capacidad { get; set; }
        public string Ciudad { get; set; }
    }
}
