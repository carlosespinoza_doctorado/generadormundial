﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorConsola.Entidades
{
    public class Mundial
    {
        public string IdMundial { get; set; }
        public int Anio { get; set; }
        public string Pais { get; set; }
        public string IdGanador { get; set; }
        public string Ganador { get; set; }
        public string IdSegundo { get; set; }
        public string Segundo { get; set; }
        public string IdTercero { get; set; }
        public string Tercero { get; set; }
        public int NumIncidentes { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
    }
}
