﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorConsola.Entidades
{
    public class Incidente
    {
        public string IdIncidente { get; set; }
        public string IdMundial { get; set; }
        public string IdPartido { get; set; }
        public DateTime Fecha { get; set;}
        public string Nivel { get; set; }
        public int Detenidos { get; set; }
        public int Hospitalizados { get; set; }
    }
}
