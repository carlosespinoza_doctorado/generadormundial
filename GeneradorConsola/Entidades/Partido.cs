﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorConsola.Entidades
{
    public class Partido
    {
        public string IdPartido { get; set; }
        public string IdMundial { get; set; }
        public string IdPais1 { get; set; }
        public string Pais1 { get; set; }
        public int GolesPais1 { get; set; }
        public string IdPais2 { get; set; }
        public string Pais2 { get; set; }
        public int GolesPais2 { get; set; }
        public string IdPaisGanador { get; set; }
        public string PaisGanador { get; set; }
        public string IdPaisPerdedor { get; set; }
        public string PaisPerdedor { get; set; }

        public string Ronda { get; set; }
        public bool FueEmpate { get; set; }
        public string IdEstadio { get; set; }
        public string CiudadEstadio { get; set; }
        public DateTime FechaHora { get; set; }
        public int Asistentes { get; set; }
    }
}
