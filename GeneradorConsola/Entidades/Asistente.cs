﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorConsola.Entidades
{
    public class Asistente
    {
        public string IdAsistente { get; set; }
        public string IdPartido { get; set; }
        public char Genero { get; set; }
        public int Edad { get; set; }
        public DateTime FechaLlegada { get; set; }
        public DateTime FechaPartida { get; set; }

    }
}
