﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorConsola.Entidades
{
    public class Grupo
    {
        public string IdMundial { get; set; }
        public string IdGrupo { get; set; }
        public string IdPais1 { get; set; }
        public string Pais1 { get; set; }
        public int PuntosPais1 { get; set; }
        public string IdPais2 { get; set; }
        public string Pais2 { get; set; }
        public int PuntosPais2 { get; set; }
        public string IdPais3 { get; set; }
        public string Pais3 { get; set; }
        public int PuntosPais3 { get; set; }
        public string IdPais4 { get; set; }
        public string Pais4 { get; set; }
        public int PuntosPais4 { get; set; }
        public string IdPaisPrimero { get; set; }
        public string Primero { get; set; }
        public string IdPaisSegundo { get; set; }
        public string Segundo { get; set; }
        public string NombreGrupo { get; set; }
    }
}
