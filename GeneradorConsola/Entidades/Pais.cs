﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneradorConsola.Entidades
{
    public class Pais
    {
        public string IdPais { get; set; }
        public string NombreCorto { get; set; }
        public string Nombre { get; set; }
    }
}
